package grupp9.pages;

import grupp9.Game;
import grupp9.TournamentRecord;
import grupp9.PageTypes;
import grupp9.personalities.*;

import java.util.*;
import java.util.stream.IntStream;

public class Statistics implements Page {

    private PageTypes nextSection;

    @Override
    public void viewPage(Humanus humanPlayer) {
         nextSection = PageTypes.STATISTICS;

        System.out.println("Which statistic would you like to view?");
        String[] options = new String[] {
                "Previous Tournaments",
                "My stats",
                "Other players stats",
                "Back to Main menu"};

        switch (ui.selectChoice(options)){
            case 1 -> printAllRecords();

            case 2 -> printPlayerStatistics(humanPlayer.getName());

            case 3 -> printOtherPlayerStatistics();

            default -> nextSection = PageTypes.MAIN;
        }
    }

    @Override
    public PageTypes getNextPageType() {
        return nextSection;
    }

    private void printAllRecords(){
        ui.newPage();
        Game.getTournamentRecords().stream()
                .sorted(Comparator.comparing(TournamentRecord::getTimePlayed))
                .forEach( x -> System.out.println(x + "\n"));
        ui.pause();
    }

    private void printPlayerStatistics(String playerName){
        ui.newPage();
        System.out.println("Player Statistics for " + playerName + ":");

        IntSummaryStatistics playerStatistics = Game.getTournamentRecords().stream()
                .map(TournamentRecord::finalPlacings)
                .mapToInt(placings -> IntStream.range(1, placings.size()+1)
                        .filter(place -> placings.get(place-1).getName().equalsIgnoreCase(playerName))
                        .findFirst().orElse(-1)
                )
                .filter(x -> x > 0)
                .summaryStatistics();

        long gamesPlayed = playerStatistics.getCount();

        if (gamesPlayed == 0){
            System.out.println(playerName + " haven't played any games.");
            ui.pause();
            return;
        }

        double averagePlacement = playerStatistics.getAverage();
        double highestPlacement = playerStatistics.getMin();
        double lowestPlacement = playerStatistics.getMax();

        System.out.println(
                "\tGames played: " + gamesPlayed + " \n" +
                        "\tAverage Placement: " + averagePlacement + "\n" +
                        "\tHighest Placement: " + highestPlacement + "\n" +
                        "\tLowest Placement: " + lowestPlacement);
        ui.pause();
    }

    private void printOtherPlayerStatistics(){
        ui.newPage();
        System.out.println("Which players statistics would you like to view?");
        System.out.print("Player name: ");
        printPlayerStatistics(ui.getNext());
    }

}
