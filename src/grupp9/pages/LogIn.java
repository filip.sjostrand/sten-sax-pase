package grupp9.pages;

import grupp9.Game;
import grupp9.PageTypes;
import grupp9.UI;
import grupp9.personalities.Humanus;

public class LogIn implements Page {

    @Override
    public void viewPage(Humanus humanPlayer) {
        System.out.println("------------------------------------");
        System.out.println("Welcome to Rock-Paper-Scissors-3000!");
        System.out.println("------------------------------------\n");
        System.out.println("Log in:");
        System.out.print("\tname: ");
        Game.playerList.remove(humanPlayer);
        humanPlayer = new Humanus(ui.getNext());
        System.out.println();
        System.out.println("Welcome " + humanPlayer.getName() + "!");
        Game.setHumanPlayer(humanPlayer);
        Game.playerList.add(humanPlayer);

    }

    @Override
    public PageTypes getNextPageType() {
        return PageTypes.MAIN;
    }


}
