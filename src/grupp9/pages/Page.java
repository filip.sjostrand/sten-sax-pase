package grupp9.pages;

import grupp9.PageTypes;
import grupp9.UI;
import grupp9.personalities.Humanus;

public interface Page {

    UI ui = UI.getUI();

    /**
     * Execute code to display the page
     * @param humanPlayer is the player that is using the page.
     */
    void viewPage(Humanus humanPlayer);

    /**
     * After viewPage() finished this will be called and should return the PageType that should be displayed after
     * this page.
     * @return PageType that should be used after this Page.
     */
    PageTypes getNextPageType();
}
