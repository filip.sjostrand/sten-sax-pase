package grupp9.pages;

import grupp9.PageTypes;
import grupp9.personalities.Humanus;

public class MainMenu implements Page {

    public MainMenu() {
    }

    @Override
    public void viewPage(Humanus humanPlayer) {
        System.out.println("Logged in as: " + humanPlayer.getName());
    }

    @Override
    public PageTypes getNextPageType() {
        System.out.println("What would you like to do?");
        String[] options = new String[] {
                "Play Tournament",
                "View Statistics",
                "Change Player",
                "Exit"};

        return switch (ui.selectChoice(options)){
            case 1 -> PageTypes.TOURNAMENT;
            case 2 -> PageTypes.STATISTICS;
            case 3 -> PageTypes.LOGIN;
            default -> PageTypes.EXIT;
        };
    }

}
