package grupp9.pages;

import grupp9.PageTypes;
import grupp9.personalities.Humanus;

public class Exit implements Page {
    @Override
    public void viewPage(Humanus humanPlayer) {

        System.out.println("--------------------------------------");
        System.out.println("Thanks for playing! Please come again.");
        System.out.println("--------------------------------------\n\n\n");
    }

    @Override
    public PageTypes getNextPageType() {
        return null;
    }
}
