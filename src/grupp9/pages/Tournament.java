package grupp9.pages;

import grupp9.*;
import grupp9.personalities.Humanus;
import grupp9.personalities.Player;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;


public class Tournament implements Page {

    @Override
    public void viewPage(Humanus humanPlayer) {


        // Tournament Bracket Setup _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ (Filip)

        // I Tournament.java finns turneringslogik för 2^x spelare.

        // Spelar-Listor
        // Alla spelare placeras i "gamerlist" -> "matchlist" -> turneringsloop.

        // Resultat-sortering
        // I turneringsloopen sorteras spelarna i "winnerlist" eller "loserlist" baserat på matchresultat.
        // (Efter omgång 1 körs loop på "winnerlist")
        // Vinnare i "winnerlist" läggs sist i "loserlist" som vänds.

        // Turneringsstatistik
        // Ett TournamentRecord-objekt skapas med Date och omvänd "loserlist" som argument.
        // Turneringsresultatet görs tillgängligt i Game-klassens main-metod för vidare hantering.

        ArrayList<Player> playerList = Game.playerList;
        Collections.shuffle(playerList);
        ArrayList<Player> gamerList = new ArrayList<>(playerList);
        ArrayList<Player> matchList = new ArrayList<>();
        ArrayList<Player> winnerList = new ArrayList<>();
        ArrayList<Player> loserList = new ArrayList<>();


        while (gamerList.size() > 1) {

            for (int i = 0; i <= gamerList.size() / 2; i++) {

                System.out.println("Tournament Rounds left: " + i);
                matchList.add(gamerList.get(0));
                matchList.add(gamerList.get(1));

                Player player1 = gamerList.get(0);
                Player player2 = gamerList.get(1);

                Match match = new Match(player1, player2);
                match.playMatch();

                winnerList.add(match.getWinner());

                gamerList.remove(match.getWinner());
                matchList.remove(match.getWinner());

                loserList.add(matchList.get(0));

                gamerList.remove(matchList.get(0));
                matchList.remove(matchList.get(0));

                if (gamerList.size() == 0 && winnerList.size() > 1) {
                    gamerList = winnerList;
                }
            }

            if (winnerList.size() == 1) {
                loserList.add(winnerList.get(0));
                gamerList.remove(winnerList.get(0));

                Collections.reverse(loserList);
                System.out.println("------------------------------------");
                System.out.println("Tournament results:");
                System.out.println("------------------------------------");
                loserList.forEach(x -> System.out.println(x.getName()));

                Date Date = new Date();
                TournamentRecord tournamentResults = new TournamentRecord(Date, loserList);
                Game.addTournamentRecord(tournamentResults);
                ui.pause();
            }
        }

// _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _

    }

    @Override
    public PageTypes getNextPageType() {
        return PageTypes.MAIN;
    }
}
