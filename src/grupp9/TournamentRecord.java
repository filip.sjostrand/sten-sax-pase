package grupp9;

import grupp9.personalities.Player;

import java.util.ArrayList;
import java.util.Date;

public record TournamentRecord(Date timePlayed, ArrayList<Player> finalPlacings) {

    public Date getTimePlayed() {
        return timePlayed;
    }

    public ArrayList<Player> getFinalPlacings() {
        return finalPlacings;
    }

    @Override
    public String toString() {
        return "Date: " + timePlayed +
                "\n\tPlacements: " + finalPlacings
                .stream()
                .map(x -> "\n\t\t" + (finalPlacings.indexOf(x)+1) + ": " + x.getName())
                .reduce("", (x,y) -> x + " " + y);
    }
}
