package grupp9;

import grupp9.personalities.Moves;
import grupp9.personalities.Player;

public class Match {

    private final Player player1;
    private final Player player2;
    private boolean player1Won;
    private int draws = 0;

    public Match(Player player1, Player player2){
        this.player1 = player1;
        this.player2 = player2;
    }

    public void playMatch(){
        Moves move1 = player1.selectMove(player2);
        Moves move2 = player2.selectMove(player1);

        if (move1.equals(move2)){
            draws++;
            if (draws < 10) {
                UI.getUI().newPage();
                System.out.println("Draw! Rematch:");
                playMatch();
            }else {
                player1Won = true;
            }
            return;
        }

        player1Won = switch (move1){
            case ROCK -> (move2 == Moves.SCISSORS);
            case PAPER -> (move2 == Moves.ROCK);
            case SCISSORS -> (move2 == Moves.PAPER);
        };

        UI.getUI().newPage();
        System.out.println("In the match between " + player1.getName() + " and " + player2.getName() + ":");
        System.out.println(player1.getName() + " chose " + move1 + "!");
        System.out.println(player2.getName() + " chose " + move2 + "!");
        if (player1Won){
            System.out.println(player1.getName() + " won the match!");
        }else{
            System.out.println(player2.getName() + " won the match!");
        }
        UI.getUI().pause();

    }

    public Player getWinner(){
        if (player1Won) return player1;
        return player2;
    }

    public Player getLoser(){
        if (player1Won) return player2;
        return player1;
    }

    public int getDraws(){
        return draws;
    }
}
