package grupp9;

import java.util.*;

// Singleton Design Pattern
public class UI {

    private static UI activeUI = null;
    private final Scanner keyboard;

    private UI(){
        keyboard = new Scanner(System.in);
    }

    public static UI getUI(){
        if (activeUI == null){
            activeUI = new UI();
        }
        return activeUI;
    }

    /***
     * Lets a user choose a string from a List of choices. returns users choice.
     * Choices are displayed to user in an ordered list indexed from 1.
     * @param choices Choices the user choose from.
     * @return index of the users choice. index starts at 1.
     */
    public int selectChoice(String[] choices){
        for (int index = 1; index <= choices.length; index++){
            System.out.println("\t" + index + ": " + choices[index-1] + ".");
        }
        String[] choicesLower = Arrays.stream(choices).map(String::toLowerCase).toArray(String[]::new);
        System.out.print("Your choice: ");
        String input = keyboard.nextLine();
        System.out.println();
        int inputInt;
        try {
            inputInt = Integer.parseInt(input);
        }
        catch (NumberFormatException ignored){
            int choice = Arrays.asList(choicesLower).indexOf(input.toLowerCase());
            if (choice == -1){
                System.out.println("\""+input+"\" is not a valid option. Please try again:");
                return selectChoice(choices);
            }
            return choice + 1;
        }

        if ( inputInt < choices.length+1 && inputInt > 0) return inputInt;
        System.out.println("\""+input+"\" is not a valid option. Please try again:");
        return selectChoice(choices);
    }

    public int selectChoice(ArrayList<String> choices){
        return selectChoice(choices.toArray(new String[0]));
    }

    public String getNext(){
        return keyboard.nextLine();
    }

    public void pause(){
        System.out.println();
        System.out.println("Press enter to continue.");
        keyboard.nextLine();
        newPage();
    }

    public void newPage(){
        System.out.println("\n".repeat(30));
    }

}
