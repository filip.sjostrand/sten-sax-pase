package grupp9.personalities;

import java.time.LocalTime;

public class Clockus extends Player {


    public Clockus(String name) {
        super(name);
    }

    /* Gets the current time and depending on the minute it will select a move for the player.
       Divides the hour in three moves. */
    private final LocalTime currentTime = LocalTime.now();
    @Override
    public Moves selectMove(Player opponent) {
        if (currentTime.getMinute() <= 20) {
            return Moves.ROCK;
        } else if (currentTime.getMinute() <= 40) {
            return Moves.PAPER;
        } else {
            return Moves.SCISSORS;
        }
    }
}
