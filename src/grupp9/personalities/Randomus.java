package grupp9.personalities;

import java.util.Random;

public class Randomus extends Player {

    public Randomus(String name) {
        super(name);
    }

    // Gets a random number between 1-3 and depending on the number it will give the player a move through the switch case.
    private final Random randomNumber = new Random();
    @Override
    public Moves selectMove(Player opponent) {
        return switch ((int) (randomNumber.nextDouble() * 3)) {
            case 0 -> Moves.ROCK;
            case 1 -> Moves.PAPER;
            case 2 -> Moves.SCISSORS;
            default -> null;
        };
    }
}
