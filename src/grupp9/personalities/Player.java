package grupp9.personalities;

public abstract class Player {


    private final String name;

    public Player(String name) {
        this.name = name;
    }

    public abstract Moves selectMove(Player opponent);

    public String getName() {
        return name;
    }
}
