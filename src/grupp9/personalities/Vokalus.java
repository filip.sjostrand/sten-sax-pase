package grupp9.personalities;


public class Vokalus extends Player {

    public Vokalus(String name) {
        super(name);
    }

    /* Checks the opponents name for the amount of vocals and depending on the modulos result, it will give the player a
       move through the switch case. */
    @Override
    public Moves selectMove(Player opponent) {
        return switch (opponent.getName()
                .toLowerCase()
                .split("aeiouy")
                .length % 3) {
            case 0 -> Moves.ROCK;
            case 1 -> Moves.PAPER;
            case 2 -> Moves.SCISSORS;
            default -> null;
        };
    }
}
