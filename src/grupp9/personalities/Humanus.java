package grupp9.personalities;

import grupp9.UI;

public class Humanus extends Player{
    public Humanus(String name) {
        super(name);
    }

    // Lets the player select a move through the switch case with help from the UI class.
    private final String[] userChoice = {"ROCK", "PAPER", "SCISSORS"};
    @Override
    public Moves selectMove(Player opponent) {
        System.out.println("It's your turn!");
        System.out.println("You're going up against: " + opponent.getName());
        System.out.println("Before the duel you will have to choose your move.");
        return switch (UI.getUI().selectChoice(userChoice)){
            case 1 -> Moves.ROCK;
            case 2 -> Moves.PAPER;
            case 3 -> Moves.SCISSORS;
            default -> null;
        };
    }
}
