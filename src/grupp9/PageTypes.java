package grupp9;

import grupp9.pages.*;
import grupp9.pages.Tournament;

public enum PageTypes {
    MAIN(new MainMenu()),
    TOURNAMENT(new Tournament()),
    STATISTICS(new Statistics()),
    EXIT(new Exit()),
    LOGIN(new LogIn());

    private final Page instance;

    PageTypes(Page instance) {
        this.instance = instance;
    }

    public static Page getInstance(PageTypes section){
        return section.instance;
    }
}
